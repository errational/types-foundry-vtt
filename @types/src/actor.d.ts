declare class Actor extends Entity {
	token: Token;

	static readonly collection: Collection;

	readonly img: string;
	readonly items: Item[];
	readonly isPC: boolean;
	readonly isToken: boolean;

	static fromToken(token: Token): Actor;

	constructor(data: object, options: object);
	getActiveTokens(linked: boolean): Token[];
	getTokenImages(): Promise<string>;
	importItemFromCollection(collection: string, entryId: string): Item;
	getOwnedItem(itemId: number): Item | null;
	createOwnedItem(itemData: object, options: object): Promise<Item>;
	updateOwnedItem(itemData: object, options? : object): Promise<Item>;
	deleteOwnedItem(itemId: number, options?: object): Promise<number>;
}
