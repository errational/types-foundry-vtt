declare class BaseEntitySheet extends FormApplication {
	readonly entity: Entity;

	constructor(object: any, options: FormApplicationOptions);

	// @ts-ignore (Different signature from FormApplication's return value.)
	getData(): { [key: string]: any };
}