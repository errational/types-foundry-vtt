declare class Canvas {
	app: PIXI.Application;
	stage: PIXI.Container;
	hud: HeadsUpDisplay;
	id: string;
	scene: string;
	dimensions: {
		width: number, sceneWidth: number,
		height: number, sceneHeight: number,
		size: number,
		distance: number,
		shiftX: number, shiftY: number,
		ratio: number
	}
	ready: boolean;
  tokens : TokenLayer;
	constructor();
}
