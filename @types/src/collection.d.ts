/// <reference types="socket.io" />

declare class Collection {
	entities: Entity[];
	source: object;
	apps: Application[];

	static readonly instance: Collection;
	static readonly directory: any;

	readonly name: string;
	readonly object: Entity;
	readonly entity: string;

	[Symbol.iterator](): Iterator<Item>;

	static socketListeners(socket: SocketIO.Socket): void;

	constructor(data: object, apps: Application[]);

	insert(entity: Entity): void;
	remove(id: string): void;
	get(id: string): Entity;
	index(id: string): number;
	render(...args: any): void;
	importFromCollection(collection: string, entryId: string): Promise<Entity>;
	create({ created, temporary }: { created: object, temporary: boolean }): Entity;
	update({ updated }: { updated: object }): Entity;
	delete({ deleted }: { deleted: string }): string;
}