declare const CONST: {
    PLAYLIST_MODES: {
        DISABLED: Number,
        SEQUENTIAL: Number,
        SHUFFLE: Number,
        SIMULTANEOUS: Number
    }
}