declare class Game {
    [x: string]: any;
	readonly user: User;
	data: {[version: string]: string};
}
