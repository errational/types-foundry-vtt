declare class Hooks {
	static on(hook: string, fn: Function): void;
	static once(hook: string, fn: Function): void;
	static off(hook: string, fn: Function): void;
	static callAll(hook: string, ...args: any): boolean;
	static call(hook: string, ...args: any): boolean;
}

declare function flattenObject(obj: object, _d: number): object;