declare class Item extends Entity {	
	// Variables
	actor: Actor;

	// Properties
	static readonly collection: Items;

	readonly img: string;
	readonly type: string;
	readonly limited: boolean;
	readonly isOwned: boolean;

	// Methods
	static createOwned(itemData: object, actor: Actor): Item;

	constructor(...args: any);
}