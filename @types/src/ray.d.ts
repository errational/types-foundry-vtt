declare class Ray {
	A: { x: number, y: number };
	B: { x: number, y: number };
	y0: number;
	x0: number;
	dx: number;
	dy: number;

	readonly slope: number;
	readonly angle: number;
	readonly distance: number;

	static fromAngle(x: number, y: number, radians: number, distance: number): Ray;
	static fromArrays(A: number[], B: number[]): Ray;
	static _getIntersection(x1: number, y1: number, x2: number, y2: number, x3: number, y3: number, x4: number, y4: number): boolean | { x: number, y: number, t0: number, t1: number };

	constructor(A: { x: number, y: number }, B: { x: number, y: number });

	project(t: number): { x: number, y: number };
	shiftAngle(angleOffset: number): Ray;
	intersectSegment(coords: number[]): boolean | { x: number, y: number, t0: number, t1: number };
}